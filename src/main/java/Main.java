import java.util.Random;
import java.util.Scanner;


public class Main {

    static Question[] questions;
    static String[][] answers;
    static int[] correctAnswers;
    static int[] points;
    static boolean[] booleans;
    static int[] usersAnswers;
    static double time = 0;
    static int totalPoint = 0;
    static double finalPoints = 0;

    public static void main(String[] args) {
        maxQuestionsAndAnswers();
        questions = new Question[maxQuestions + 1];
        instruction();
    }

    private static void instruction() {
        System.out.println("Список команд:" + "\n" + "/add -добавление вопросов");
        System.out.println("/start -начать тестирование");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        if (command.equals("/add")) {
            addedQuestion();
        }
        if (command.equals("/start")) {
            testRun();
        }
        instruction();
    }

    static int counterQuestion = 0;

    private static void addedQuestion() {
        time = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cколько вопросов вы хотите добавить?");
        x = scanner.nextInt();

        for (int i = 1; i < x + 1; i++) {
            System.out.println("Введите вопрос");
            String question = scanner.nextLine();
            question = scanner.nextLine();
            System.out.println("Сколько баллов за данный вопрос?");
            int y = scanner.nextInt();
            for (int j = 1; j <maxQuestions+1 ; j++) {
                if (points[j] == 0) {
                    points[j] = y;
                    break;
                }
            }

            counterQuestion += 1;
            questions[counterQuestion] = new Question(question);
            addedAnswers();
        }
        instruction();
    }


    static private byte n = 1;

    private static void addedAnswers() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество ответов в вопросе");
        int countAnswers = scanner.nextInt();

        answers[counterQuestion][1] = scanner.nextLine();
        byte counter = 1;
        for (int i = 1; i <= countAnswers; i++) {
            System.out.println("Введите " + counter + " ответ");
            answers[n][i] = scanner.nextLine();
            counter += 1;
        }

        System.out.println("Какие из этих вопросов верные?" + "\n" + "Ввести без пробелов номера");
        int correct = scanner.nextInt();
        correctAnswers[n] = correct;
        n += 1;
    }

    static int maxQuestions;
    static int maxAnswers;

    private static void maxQuestionsAndAnswers() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите максимальное количество вопросов в тесте");
        maxQuestions = scanner.nextInt();
        maxAnswers = 9;
        answers = new String[maxQuestions + 1][maxAnswers + 1];
        correctAnswers = new int[maxQuestions + 1];
        points = new int[maxQuestions + 1];
        booleans = new boolean[maxQuestions + 1];
        usersAnswers = new int[maxQuestions + 1];
        booleansReset();
        for (int i = 1; i < maxQuestions + 1; i++) {
            correctAnswers[i] = 0;
            usersAnswers[i] = 0;
        }

    }

    private static void booleansReset() {
        for (int i = 1; i < maxQuestions + 1; i++) {
            booleans[i] = true;
        }
    }

    private static void testRun() {
        enterTheNumberOfQuestions();
    }

    static int l = 0;
    static int x = 0;

    private static void enterTheNumberOfQuestions() {
        booleansReset();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Всего вопросов " + (counterQuestion));
        if (counterQuestion == 0) {
            System.out.println("<<Вы не составили тест>>");
            instruction();
        }
        System.out.println("Выберете количество вопросов в тесте");
        x = scanner.nextInt();
      l = x;
        int c = 0;
        for (int i = 0; i < questions.length; i++) {
            if (questions[i] != null) {
                c += 1;
            }
            ;
        }
        if (l > c) {
            System.out.println("У вас меньше вопросов");
            enterTheNumberOfQuestions();
        } else {
            int p = l;
            outputOfAnswers(p);
        }

    }

    static int rand;

    private static void outputOfAnswers(int p) {
        Random r = new Random();
        rand = r.nextInt(p) + 1;
        if (l > 0) {
            for (int i = 1; i < maxQuestions + 1; i++) {
                if (booleans[rand]) {
                    booleans[rand] = false;
                    l -= 1;
                    enteringUserAnswers(p);
                    break;
                } else {
                    outputOfAnswers(p);
                }
            }
        }
        checkingTheAnswersAndScoringPoints();
    }

    private static void enteringUserAnswers(int p) {
        double n = System.currentTimeMillis();
        Scanner scanner = new Scanner(System.in);
        System.out.println(questions[rand].getQuestion());
        int count = 1;
        for (int j = 1; j < maxAnswers + 1; j++) {
            if (answers[rand][j] != null)
                System.out.println(count + ") " + answers[rand][j]);
            count += 1;
        }
        System.out.println("Введите ваши ответы без пробелов");
        int answer = scanner.nextInt();
        usersAnswers[rand] = answer;
        time += System.currentTimeMillis() - n;
        outputOfAnswers(p);

    }


    private static void checkingTheAnswersAndScoringPoints() {
        Scanner scanner = new Scanner(System.in);
        double userPoints = 0;
        int[] inCorrectAnswers = new int[maxQuestions + 1];
        for (int i = 1; i < maxQuestions + 1; i++) {
            inCorrectAnswers[i] = 0;
        }
        for (int i = 1; i < maxQuestions + 1; i++) {
            if (correctAnswers[i] == usersAnswers[i]) {
                finalPoints = finalPoints + points[i];
            } else {
                inCorrectAnswers[i] = 1;

                int correctAnswer = correctAnswers[i];
                int userAnswer = usersAnswers[i];
                double countCorrectAnswer = 0;
                double countUserAnswer = 0;
                while (correctAnswer % 10 > 0) {
                    countCorrectAnswer = countCorrectAnswer + 1;
                    correctAnswer /= 10;
                }
                while (userAnswer % 10 > 0) {
                    countUserAnswer = countUserAnswer + 1;
                    userAnswer /= 10;
                }
                correctAnswer = correctAnswers[i];
                userAnswer = usersAnswers[i];
                if (countCorrectAnswer < countUserAnswer | countCorrectAnswer > countUserAnswer | countCorrectAnswer == countUserAnswer) {
                    while (correctAnswer % 10 > 0) {
                        userAnswer = usersAnswers[i];
                        while (userAnswer % 10 > 0) {
                            if ((userAnswer % 10) == (correctAnswer % 10)) {
                                userPoints = userPoints + points[i];
                                userAnswer /= 10;
                            } else {
                                userAnswer /= 10;
                            }
                        }
                        correctAnswer /= 10;
                    }
                    if (countCorrectAnswer < countUserAnswer) {
                        finalPoints = finalPoints + (userPoints / countUserAnswer);
                    }
                    if (countCorrectAnswer > countUserAnswer) {
                        finalPoints = finalPoints + (userPoints / countCorrectAnswer);
                    }
                    if (countCorrectAnswer == countUserAnswer) {
                        System.out.println(userPoints);
                        System.out.println(countUserAnswer);
                        finalPoints = finalPoints + (userPoints / countUserAnswer);
                    }

                    userPoints = 0;

                }
            }
        }

        for (int i = 0; i < maxQuestions; i++) {
            totalPoint = totalPoint + points[i];
        }
        System.out.println("Вы получили:" + finalPoints + " из " + totalPoint);
        System.out.println("Время, за которое вы вы выполнили тест:" + time / 1000 + " cек");
        System.out.println("Если хотите узнать ошибки: /mistake");
        System.out.println("Пройти тестирование еще раз: /start");
        System.out.println("Открыть инструкцию: /instruction");
        System.out.println("Если хотите добавить вопросы: /add");
        time = 0;
        totalPoint = 0;
        finalPoints = 0;
        String string = scanner.nextLine();
        if (string.equals("/instruction")) {
            instruction();
        }
        if (string.equals("/start")) {
            testRun();
        }
        if (string.equals("/mistake")) {
            mistakeOutPut(inCorrectAnswers);
        }
        if (string.equals("/add")){
            addedQuestion();
        }

    }

    private static void mistakeOutPut(int[] inCorrectAnswers) {

        int k = 0;
        for (int i = 1; i < maxQuestions + 1; i++) {
            if (inCorrectAnswers[i] == 0) {
                k += 1;
            }
            if (inCorrectAnswers[i] == 1) {
                System.out.println(questions[i].getQuestion());
                for (int j = 1; j < maxAnswers + 1; j++) {
                    if (answers[i][j] != null) {
                        System.out.println(j + ")" + answers[i][j]);
                    }
                }
                System.out.println("Вы ввели: " + usersAnswers[i]);
                System.out.println("Правильный ответ: " + correctAnswers[i]);

            }
        }
        if (k == maxQuestions) {
            System.out.println("Ошибок нет");
        }
        instruction();
    }
}








